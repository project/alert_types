<?php

namespace Drupal\Tests\alert_types\Functional;

use Drupal\alert_types\Entity\AlertType;
use Drupal\Tests\BrowserTestBase;

/**
 * Ensures that alert type functions work correctly.
 *
 * @group alert_types
 */
class AlertTypeTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['alert_types'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests creating an alert type via a form.
   */
  public function testAlertTypeCreationAndModification() {
    // Log in a test user.
    $web_user = $this->drupalCreateUser([
      'administer alert types'
    ]);
    $this->drupalLogin($web_user);

    $this->drupalGet('admin/structure/alert_type/add');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->pageTextContains('Label');
    $this->assertSession()->pageTextContains('Description');

    // Create a new alert type.
    $edit = [
      'label' => 'Foo',
      'id' => 'foo',
      'description' => 'Lorem ipsum',
    ];

    $this->submitForm($edit, 'Save');

    $this->drupalGet('admin/structure/alert_type');
    $this->assertSession()->pageTextContains('Foo');
    $this->assertSession()->pageTextContains('Lorem ipsum');
    $this->clickLink('Edit');

    $this->assertSession()->pageTextContains('Foo');
    $this->assertSession()->pageTextContains('Lorem ipsum');

    // Change the label and the description.
    $edit = [
      'label' => 'Bar',
      'description' => 'Lorem ipsum dolor sit amet',
    ];
    $this->submitForm($edit, 'Save');

    $this->assertSession()->pageTextContains('Bar');
    $this->assertSession()->pageTextContains('Lorem ipsum dolor sit amet');
  }

  /**
   * Tests deleting an alert type.
   */
  public function testAlertTypeDeletion() {
    $web_user = $this->drupalCreateUser([
      'administer alert types'
    ]);
    $this->drupalLogin($web_user);

    // Add a new node of this type.
    $type = AlertType::create([
      'label' => 'Foo',
      'id' => 'foo',
      'description' => 'Lorem ipsum',
      'behaviors' => [],
    ]);
    $type->save();
    // Attempt to delete the content type, which should not be allowed.
    $this->drupalGet('admin/structure/alert_type/' . $type->id() . '/delete');
    $this->assertSession()->pageTextContains("Are you sure you want to delete the alert type {$type->label()}?");
    $this->assertSession()->pageTextContains('This action cannot be undone.');

    $this->submitForm([], 'Delete');
    $this->assertSession()->pageTextContains("Alert type deleted: {$type->label()}.");
    $this->assertSession()->pageTextContains('There are no alert types yet.');

  }

  /**
   * Tests permissions for accessing alert types.
   */
  public function testAlertTypePermissions() {
    $type = AlertType::create([
      'label' => 'Foo',
      'id' => 'foo',
      'description' => 'Lorem ipsum',
      'behaviors' => [],
    ]);
    $type->save();

    // Create an admin user who can manage alert types.
    $admin_user_1 = $this->drupalCreateUser([
      'administer alert types',
    ]);
    $this->drupalLogin($admin_user_1);

    $this->drupalGet('admin/structure/alert_type');
    $this->assertSession()->statusCodeEquals(200);

    // Create another admin user who can't manage alert types.
    $admin_user_2 = $this->drupalCreateUser([
      'administer site configuration'
    ]);
    $this->drupalLogin($admin_user_2);

    $this->drupalGet('admin/structure/alert_type');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('admin/structure/alert_type/add');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('admin/structure/alert_type/' . $type->id());
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('admin/structure/alert_type/' . $type->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);

  }

}
