<?php

namespace Drupal\Tests\alert_types\Functional;

use Drupal\alert_types\Entity\AlertType;
use Drupal\alert_types\Entity\Alert;
use Drupal\Tests\BrowserTestBase;

/**
 * Ensures that alert functions work correctly.
 *
 * @group alert_types
 */
class AlertTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['alert_types'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp():void {
    parent::setup();
    AlertType::create([
      'id' => 'alert_test',
      'label' => 'Alert test',
      'behaviors' => [],
    ])->save();
  }

  /**
   * Tests creating an alert via a form.
   */
  public function testAlertCreationAndModification() {
    // Log in a test user.
    $web_user = $this->drupalCreateUser([
      'administer alert entities',
      'add alert entities',
      'edit alert entities',
    ]);
    $this->drupalLogin($web_user);

    $this->drupalGet('alert/add/alert_test');
    $this->assertSession()->statusCodeEquals(200);

    // Create a new alert.
    $edit = [
      'name[0][value]' => 'Foo',
      'message[0][value]' => 'Lorem ipsum',
      'status[value]' => TRUE,
    ];

    $this->submitForm($edit, 'Save');

    $this->assertSession()->pageTextContains('Foo');
    $this->assertSession()->pageTextContains('Active');

    $this->assertSession()->pageTextContains('Edit');
    // Edit that alert.
    $this->clickLink('Edit');

    $edit = [
      'name[0][value]' => 'Foo bar',
      'message[0][value]' => 'Lorem ipsum dolor sit amet',
      'status[value]' => FALSE,
    ];
    $this->submitForm($edit, 'Save');

    $this->assertSession()->pageTextContains('Foo bar');
    $this->assertSession()->pageTextContains('Inactive');

  }

  /**
   * Tests deleting an alert.
   */
  public function testAlertDeletion() {
    $alert = Alert::create([
      'type' => 'alert_test',
      'name' => 'Foo',
      'message' => 'Lorem ipsum',
      'status' => TRUE,
    ]);
    $alert->save();

    // Log in a test user.
    $web_user = $this->drupalCreateUser([
      'administer alert entities',
      'delete alert entities',
    ]);
    $this->drupalLogin($web_user);

    // Attempt to delete the content type, which should not be allowed.
    $this->drupalGet('alert/' . $alert->id() . '/delete');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->pageTextContains("Are you sure you want to delete the alert {$alert->label()}?");
    $this->assertSession()->pageTextContains('This action cannot be undone.');
    $this->submitForm([], 'Delete');

    $this->assertSession()->pageTextContains("The Alert {$alert->label()} has been deleted.");
    $this->assertSession()->pageTextContains('No Alerts Found.');
  }

  /**
   * Tests permissions for accessing alerts.
   */
  public function testAlertPermissions() {
    $alert = Alert::create([
      'type' => 'alert_test',
      'name' => 'Foo',
      'message' => 'Lorem ipsum',
      'status' => TRUE,
    ]);
    $alert->save();

    // Create an admin user who can manage alert types.
    $admin_user_1 = $this->drupalCreateUser([
      'administer alert entities',
    ]);
    $this->drupalLogin($admin_user_1);

    $this->drupalGet('admin/content/alerts');
    $this->assertSession()->statusCodeEquals(200);

    // Create another admin user who can't manage alert types.
    $admin_user_2 = $this->drupalCreateUser([
      'administer site configuration'
    ]);
    $this->drupalLogin($admin_user_2);

    $this->drupalGet('admin/content/alerts');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('alert/add/alert_test');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('alert/' . $alert->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('alert/' . $alert->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);

  }

}
