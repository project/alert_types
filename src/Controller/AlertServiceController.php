<?php

namespace Drupal\alert_types\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Datetime\DateFormatter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class AlertServiceController.
 *
 * @package Drupal\alert_types\Controller
 */
class AlertServiceController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * The entity view builder.
   *
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   */
  protected $viewBuilder;

  /**
   * AlertServiceController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, DateFormatter $date_formatter, Renderer $renderer) {
    $this->entityTypeManager = $entity_type_manager;
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
    $this->viewBuilder = $this->entityTypeManager->getViewBuilder('alert');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Fetches all the release data and returns them with appropriate context
   * data.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A json response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function json() {
    $entities = $this->entityTypeManager->getStorage('alert')->loadActive();

    $items = [];
    // We need to render the full HTML markup before sending it to the JSON response.
    foreach ($entities as $entity) {
      $items[] = [
        'id' => $entity->id(),
        'content' => $this->renderAlert($entity),
      ];
    }

    return new JsonResponse([
      'data' => $items,
      'status' => 200,
      'method' => 'GET',
    ]);
  }

  /**
   * Render the full markup for an alert.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The alert entity.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The alert markup.
   *
   * @throws \Exception
   */
  protected function renderAlert(EntityInterface $entity) {
    $render_array = $this->viewBuilder->view($entity);
    return $this->renderer->render($render_array);
  }

}
