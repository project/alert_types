<?php

namespace Drupal\alert_types\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\alert_types\Entity\AlertInterface;

/**
 * Class AlertController.
 *
 *  Returns responses for Alert routes.
 */
class AlertController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Constructs a AlertController object.
   *
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer service.
   */
  public function __construct(DateFormatter $date_formatter, Renderer $renderer) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Displays an Alert revision.
   *
   * @param int $alert_revision
   *   The Alert revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($alert_revision) {
    $alert = $this->entityTypeManager()->getStorage('alert')->loadRevision($alert_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('alert');

    return $view_builder->view($alert);
  }

  /**
   * Page title callback for a Alert  revision.
   *
   * @param int $alert_revision
   *   The Alert revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($alert_revision) {
    $alert = $this->entityTypeManager()->getStorage('alert')->loadRevision($alert_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $alert->label(),
      '%date' => $this->dateFormatter->format($alert->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Alert .
   *
   * @param \Drupal\alert_types\Entity\AlertInterface $alert
   *   A Alert  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(AlertInterface $alert) {
    $account = $this->currentUser();
    $langcode = $alert->language()->getId();
    $langname = $alert->language()->getName();
    $languages = $alert->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $alert_storage = $this->entityTypeManager()->getStorage('alert');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $alert->label()]) : $this->t('Revisions for %title', ['%title' => $alert->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all alert revisions") || $account->hasPermission('administer alert entities')));
    $delete_permission = (($account->hasPermission("delete all alert revisions") || $account->hasPermission('administer alert entities')));

    $rows = [];

    $vids = $alert_storage->revisionIds($alert);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\alert_types\AlertInterface $revision */
      $revision = $alert_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $alert->getRevisionId()) {
          $link = Link::fromTextAndUrl($date, new Url('entity.alert.revision', ['alert' => $alert->id(), 'alert_revision' => $vid]));
        }
        else {
          $link = $alert->toLink($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link->toString(),
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            if ($has_translations) {
              $url = Url::fromRoute('entity.alert.translation_revert', [
                'alert' => $alert->id(),
                'alert_revision' => $vid,
                'langcode' => $langcode,
              ]);
            }
            else {
              $url = Url::fromRoute('entity.alert.revision_revert', [
                'alert' => $alert->id(),
                'alert_revision' => $vid,
              ]);
            }
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $url,
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.alert.revision_delete', [
                'alert' => $alert->id(),
                'alert_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['alert_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
