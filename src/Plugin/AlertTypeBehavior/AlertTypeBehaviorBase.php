<?php

namespace Drupal\alert_types\Plugin\AlertTypeBehavior;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Entity\EntityInterface;

/**
 * Alert type behavior base.
 */
abstract class AlertTypeBehaviorBase extends PluginBase implements AlertTypeBehaviorInterface {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(EntityInterface $entity) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function preprocess(&$variables) { }

}
