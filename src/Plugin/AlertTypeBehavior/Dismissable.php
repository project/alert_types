<?php

namespace Drupal\alert_types\Plugin\AlertTypeBehavior;

use Drupal\Core\Entity\EntityInterface;

/**
 * Add user dismissable behavior.
 *
 * @AlertTypeBehavior(
 *  id = "dismissable",
 *  label = @Translation("Dismissable"),
 *  description = @Translation("Allow users to dismiss alerts."),
 *  library = "alert_types/dismissable",
 * )
 */
class Dismissable extends AlertTypeBehaviorBase {

  /**
   * {@inheritdoc}
   */
  public function preprocess(&$variables) {
    $alert = $variables['alert'];

    // Process the dismissable value. Need to ensure that it returns
    // its value as boolean.
    if (!empty($alert->get('dismissable')->first())) {
      $dismissable = $alert->get('dismissable')->first()->getValue();
      if (!empty($dismissable['value'])) {
        $variables['dismissable'] = TRUE;
      }
    }
  }
}
