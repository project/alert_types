<?php

namespace Drupal\alert_types\Plugin\AlertTypeBehavior;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Display\EntityDisplayInterface;

/**
 * Provides an interface defining an alert type behavior plugin.
 */
interface AlertTypeViewBehaviorInterface extends AlertTypeBehaviorInterface {

  /**
   * Operate on an alert when it's being viewed.
   *
   * @param array &$build
   *   A renderable array representing the alert.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The alert entity.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The entity view display holding the display options configured for the
   *   entity components.
   * @param string $view_mode
   *   The view mode the entity is rendered in.
   *
   * @return array
   *   A render array provided by the plugin.
   */
  public function view(array &$build, EntityInterface $entity, EntityDisplayInterface $display, $view_mode);

}
