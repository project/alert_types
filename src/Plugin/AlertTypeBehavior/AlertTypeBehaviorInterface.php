<?php

namespace Drupal\alert_types\Plugin\AlertTypeBehavior;

use Drupal\Core\Entity\EntityInterface;

/**
 * Provides an interface defining an alert type behavior plugin.
 */
interface AlertTypeBehaviorInterface {

  /**
   * Determine whether this behavior is applicable.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The alert entity.
   *
   * @return bool
   *   TRUE if this is applicable; FALSE otherwise.
   */
  public static function isApplicable(EntityInterface $entity);

  /**
   * Preprocess the alert entity.
   *
   * @param array $variables
   *   An associative array of variables.
   */
  public function preprocess(array &$variables);

}
