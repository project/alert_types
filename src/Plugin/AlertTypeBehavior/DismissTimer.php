<?php

namespace Drupal\alert_types\Plugin\AlertTypeBehavior;

/**
 * Add Dissmiss Timer behavior.
 *
 * @AlertTypeBehavior(
 *  id = "dismiss_timer",
 *  label = @Translation("Dismiss Timer"),
 *  description = @Translation("Alerts will automatically be dismissed after a specified amount of time."),
 *  library = "alert_types/dismiss_timer",
 * )
 */
class DismissTimer extends AlertTypeBehaviorBase {

  /**
   * {@inheritdoc}
   */
  public function preprocess(&$variables) {
    $alert = $variables['alert'];

    // Process the dismiss_timer value. Need to ensure that it returns its value
    // as an integer.
    if (!empty($alert->get('dismiss_timer')->first())) {
      $dismiss = $alert->get('dismiss_timer')->first()->getValue();
      if (!empty($dismiss['value'])) {
        $variables['dismiss_timer'] = intval($dismiss['value']);
      }
    }

    $variables['attributes']['data-dismiss-timer'] = $variables['dismiss_timer'];
  }

}
