<?php

namespace Drupal\alert_types\Plugin\Block;

use Drupal\alert_types\AlertTypeBehaviorManager;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an Alerts service block.
 *
 * @Block(
 *   id = "alerts_block",
 *   admin_label = @Translation("Alerts"),
 * )
 */
class AlertServiceBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The alert type behavior manager.
   *
   * @var \Drupal\alert_types\AlertTypeBehaviorManager
   */
  protected $behaviorManager;

  /**
   * Constructs an AlertServiceBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\alert_types\AlertTypeBehaviorManager $behavior_manager
   *   The alert type behavior manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, AlertTypeBehaviorManager $behavior_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->behaviorManager = $behavior_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.alert_type_behavior'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $entities = $this->entityTypeManager->getStorage('alert')->loadActive();

    $alerts = [];
    // Make sure that this block cache gets invalidated every time an alert is
    // added, updated or deleted.
    $cache_tags = ['alert_list'];

    foreach ($entities as $entity) {
      // We're doing access here because it's difficult to get the context
      // for content type and current page when loading content via javascript.
      // We're keeping the javascript though because it helps with browser cache
      // and gives us a better experience.
      if ($entity->access('view', \Drupal::currentUser())) {
        $alerts[] = $entity->id();
        $cache_tags = array_merge($cache_tags, $entity->getCacheTags());
      }
    }

    $behaviors = $this->getAlertTypesBehaviorLibraries($entities);
    $libraries = array_merge(['alert_types/alerts'], $behaviors);

    return [
      '#theme' => 'alert_block',
      '#attached' => [
        'library' => $libraries,
        'drupalSettings' => [
          'alerts' => $alerts,
        ],
      ],
      '#cache' => [
        'tags' => $cache_tags,
        'contexts' => [
          'url.path',
          'user',
        ],
      ],
    ];
  }

  /**
   * Return the libraries for all the applicable alert types.
   *
   * @return array
   *   The array of libraries correlating with behavior plugins.
   */
  protected function getAlertTypesBehaviorLibraries(array $entities) {
    $bundles = $this->getEntityBundles($entities);

    $definitions = $this->behaviorManager->getDefinitionsByAlertTypes($bundles);
    $libraries = [];
    foreach ($definitions as $definition) {
      $libraries[] = $definition['library'];
    }
    return array_unique($libraries);
  }

  /**
   * Get all the current entity bundles.
   *
   * @param array $entities
   *   Array of alert entities.
   *
   * @return array
   *   The array of bundles.
   */
  protected function getEntityBundles(array $entities) {
    // Get all the bundles.
    $bundles = [];
    foreach ($entities as $entity) {
      $bundles[] = $entity->bundle();
    }
    return $bundles;
  }

}
