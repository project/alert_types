<?php

namespace Drupal\alert_types;

use Drupal\alert_types\Plugin\AlertTypeBehavior\AlertTypeViewBehaviorInterface;
use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Entity\EntityInterface;

/**
 * Alert Type Behavior plugin manager.
 */
class AlertTypeBehaviorManager extends DefaultPluginManager {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The alert types behavior plugin manager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct('Plugin/AlertTypeBehavior', $namespaces, $module_handler, 'Drupal\alert_types\Plugin\AlertTypeBehavior\AlertTypeBehaviorInterface', 'Drupal\alert_types\Annotation\AlertTypeBehavior');

    $this->alterInfo('alert_type_behavior');
    $this->setCacheBackend($cache_backend, 'alert_type_behavior');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get the enabled behaviors for an alert type.
   *
   * @param string $bundle
   *   The alert bundle.
   *
   * @return array
   *   The plugin definitions.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getDefinitionsByAlertTypes(array $bundles) {
    $alert_types = $this->entityTypeManager->getStorage('alert_type')->loadMultiple($bundles);
    $behaviors = [];
    foreach ($alert_types as $type) {
      $current_behaviors = $type->getBehaviors() ?? [];
      $filtered_behaviors = array_keys(array_filter($current_behaviors));
      $behaviors = array_merge($behaviors, $filtered_behaviors);
    }
    $behaviors = array_unique($behaviors);

    $definitions = [];
    foreach ($behaviors as $behavior) {
      $definitions[] = $this->getDefinition($behavior);
    }

    return $definitions;
  }

  /**
   * Get all applicable behavior plugins.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The alert entity.
   *
   * @return AlertTypeViewBehaviorInterface[]
   *   Array of behavior plugin instances.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getApplicableBehaviors(EntityInterface $entity) {
    $definitions = $this->getDefinitionsByAlertTypes([$entity->bundle()]);
    $behaviors = [];
    foreach ($definitions as $definition) {
      $plugin_class = DefaultFactory::getPluginClass($definition['id'], $definition);
      if ($plugin_class::isApplicable($entity)) {
        $behaviors[] = $this->createInstance($definition['id']);
      }
    }

    return $behaviors;
  }

}
