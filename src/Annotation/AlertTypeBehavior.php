<?php

namespace Drupal\alert_types\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an item setting annotation object.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class AlertTypeBehavior extends Plugin {

  /**
   * The alert type behavior ID.
   *
   * @var string
   */
  public $id;

  /**
   * The alert type behavior label.
   *
   * @var string
   */
  public $label;

  /**
   * The description of the alert type behavior.
   *
   * @var
   */
  public $description;

  /**
   * The javascript or css library.
   *
   * @var
   */
  public $library;

  /**
   * Provide an event when you want this behavior to fire.
   *
   * @var
   */
  public $event;

}
