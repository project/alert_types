<?php

namespace Drupal\alert_types\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Alert entities.
 *
 * @ingroup alert_types
 */
class AlertDeleteForm extends ContentEntityDeleteForm {

}
