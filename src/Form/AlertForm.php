<?php

namespace Drupal\alert_types\Form;

use Drupal\Core\Condition\ConditionPluginCollection;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;

/**
 * Form controller for Alert edit forms.
 *
 * @ingroup alert_types
 */
class AlertForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $contextRepository = \Drupal::service('context.repository');
    $form_state->setTemporaryValue('gathered_contexts', $contextRepository->getAvailableContexts());

    $form['conditions_tab'] = [
      '#type' => 'details',
      '#title' => $this->t('Visibility'),
      '#group' => 'advanced',
    ];

    $form['conditions_tab']['conditions'] = $this->buildConditionsInterface([], $form_state);

    return $form;
  }

  /**
   * Helper function for building the conditions UI form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form array with the conditions UI added in.
   */
  protected function buildConditionsInterface(array $form, FormStateInterface $form_state) {

    $form['#tree'] = TRUE;
    // Currently only supporting these conditions.
    $supported = [
     'entity_bundle:node',
     'user_role',
     'request_path',
    ];
    $conditions = $this->entity->getConditions();

    $manager = \Drupal::service('plugin.manager.condition');
    foreach ($manager->getDefinitionsForContexts($form_state->getTemporaryValue('gathered_contexts')) as $condition_id => $definition) {
     if (in_array($condition_id, $supported)) {
       $condition_config = isset($conditions[$condition_id]) ? $conditions[$condition_id] : [];
       $condition = $manager->createInstance($condition_id, $condition_config);
       $form_state->set(['conditions', $condition_id], $condition);
       $condition_form = $condition->buildConfigurationForm([], $form_state);
       $condition_form['#type'] = 'details';
       $condition_form['#title'] = $condition->getPluginDefinition()['label'];
       $condition_form['#group'] = 'conditions_tab';
       $form[$condition_id] = $condition_form;
     }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // Normalizing the condition results.
    $conditionPluginManager = \Drupal::service('plugin.manager.condition');
    $conditionCollection = new ConditionPluginCollection($conditionPluginManager, $form_state->getValue('conditions'));
    foreach ($form_state->getValue('conditions') as $condition_id => $values) {
      $condition = $form_state->get(['conditions', $condition_id]);
      $condition->submitConfigurationForm($form['conditions_tab']['conditions'][$condition_id], SubformState::createForSubform($form['conditions_tab']['conditions'][$condition_id], $form, $form_state));
      $condition_configuration = $condition->getConfiguration();
      // Drupal core bug fix workaround, make sure that negate is always a boolean.
      if (isset($condition_configuration['negate'])) {
        $condition_configuration['negate'] = (bool) $condition_configuration['negate'];
      }
      $conditionCollection->addInstanceId($condition_id, $condition_configuration);
    }
    $conditions = $conditionCollection->getConfiguration();

    $entity->setConditions($conditions);

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('revision') && $form_state->getValue('revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime(\Drupal::time()->getRequestTime());
      $entity->setRevisionUserId($this->currentUser()->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        \Drupal::messenger()->addStatus($this->t('Created the %label Alert.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        \Drupal::messenger()->addStatus($this->t('Saved the %label Alert.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.alert.collection');
  }

}
