<?php

namespace Drupal\alert_types\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AlertTypeForm.
 */
class AlertTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $behaviorPlugins = \Drupal::service('plugin.manager.alert_type_behavior');

    $form = parent::form($form, $form_state);

    $alert_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $alert_type->label(),
      '#description' => $this->t("The human-readable name of this alert type. This text will be displayed as part of the list on the Add alert page. This name must be unique."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $alert_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\alert_types\Entity\AlertType::load',
      ],
      '#disabled' => !$alert_type->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#maxlength' => 255,
      '#default_value' => $alert_type->getDescription(),
      '#description' => $this->t("This text will be displayed on the Add new alert page."),
    ];

    $definitions = $behaviorPlugins->getDefinitions();
    if (!empty($definitions)) {
      $form['behaviors'] = [
        '#type' => 'details',
        '#title' => $this->t('Behaviors'),
        '#description' => $this->t('Choose which behaviors are available for this alert type.'),
        '#tree' => TRUE,
      ];

      $selected_behaviors = $alert_type->getBehaviors();
      foreach ($definitions as $definition) {
        $form['behaviors'][$definition['id']] = [
          '#type' => 'checkbox',
          '#title' => $definition['label'],
          '#description' => $definition['description'],
          '#default_value' => (isset($selected_behaviors[$definition['id']])),
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $alert_type = $this->entity;

    // Remove any behaviors that weren't selected.
    $behaviors = array_filter($alert_type->getBehaviors());
    $alert_type->setBehaviors($behaviors);

    $status = $alert_type->save();

    switch ($status) {
      case SAVED_NEW:
        \Drupal::messenger()->addStatus($this->t('Created the %label Alert type.', [
          '%label' => $alert_type->label(),
        ]));
        break;

      default:
        \Drupal::messenger()->addStatus($this->t('Saved the %label Alert type.', [
          '%label' => $alert_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($alert_type->toUrl('collection'));
  }

}
