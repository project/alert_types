<?php

namespace Drupal\alert_types;

use Drupal\alert_types\Plugin\AlertTypeBehavior\AlertTypeViewBehaviorInterface;
use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Display\EntityDisplayInterface;

/**
 *
 */
class AlertTypeBehaviorOperations implements ContainerInjectionInterface {

  /**
   * The alert type behavior manager.
   *
   * @var \Drupal\alert_types\AlertTypeBehaviorManager
   */
  protected $behaviorManager;

  /**
   * Constructs a new EntityOperations object.
   *
   * @param \Drupal\alert_types\AlertTypeBehaviorManager $behavior_manager
   *   The alert type behavior manager.
   */
  public function __construct(AlertTypeBehaviorManager $behavior_manager) {
    $this->behaviorManager = $behavior_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.alert_type_behavior'),
    );
  }

  /**
   * Allow behavior plugins to manipulate preprocess the alert entity.
   *
   * @param array $variables
   *   The array of variables.
   *
   * @see hook_preprocess_HOOK()
   */
  public function preprocessAlert(array &$variables) {
    $behaviors = $this->behaviorManager->getApplicableBehaviors($variables['alert']);
    foreach ($behaviors as $behavior) {
      $behavior->preprocess($variables);
    }
  }

  /**
   * Acts on an entity being viewed.
   *
   * @param array $build
   *   The build render array.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being viewed.
   * @param \Drupal\Core\Entity\Display\EntityDisplayInterface $display
   *   The entity display object.
   * @param $view_mode
   *   The current view mode.
   *
   * @see hook_entity_type_view()
   */
  public function entityTypeView(array &$build, EntityInterface $entity, EntityDisplayInterface $display, $view_mode) {
    $behaviors = $this->behaviorManager->getApplicableBehaviors($entity);
    foreach ($behaviors as $behavior) {
      if ($behavior instanceof AlertTypeViewBehaviorInterface) {
        $behavior->view($build, $entity, $display, $view_mode);
      }
    }
  }

}
