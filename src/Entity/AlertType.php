<?php

namespace Drupal\alert_types\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Alert type entity.
 *
 * @ConfigEntityType(
 *   id = "alert_type",
 *   label = @Translation("Alert type"),
 *   label_collection = @Translation("Alert types"),
 *   label_singular = @Translation("Alert type"),
 *   label_plural = @Translation("alert types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count alert type",
 *     plural = "@count alert types"
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\alert_types\AlertTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\alert_types\Form\AlertTypeForm",
 *       "edit" = "Drupal\alert_types\Form\AlertTypeForm",
 *       "delete" = "Drupal\alert_types\Form\AlertTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\alert_types\AlertTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "alert_type",
 *   admin_permission = "administer alert types",
 *   bundle_of = "alert",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/alert_type/{alert_type}",
 *     "add-form" = "/admin/structure/alert_type/add",
 *     "edit-form" = "/admin/structure/alert_type/{alert_type}",
 *     "delete-form" = "/admin/structure/alert_type/{alert_type}/delete",
 *     "collection" = "/admin/structure/alert_type"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "behaviors",
 *   }
 * )
 */
class AlertType extends ConfigEntityBundleBase implements AlertTypeInterface {

  /**
   * The Alert type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Alert type label.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of this alert type.
   *
   * @var string
   */
  protected $description;

  /**
   * The enabled behaviors with this alert type.
   *
   * @var string
   */
  protected $behaviors = [];

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * {@inheritdoc}
   */
  public function getBehaviors() {
    return $this->behaviors;
  }

  /**
   * {@inheritdoc}
   */
  public function setBehaviors(array $behaviors) {
    $this->behaviors = $behaviors;
  }
}
