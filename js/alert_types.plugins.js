/**
 *  Alert Types Plugins
 *
 *  You can add custom javascript behavior to your alerts through plugins. You're
 *  able to preprocess the dom of an alerts as well as bind custom events to it.
 *  You can also provide logic to exclude that alert entirely.
 *
 *  Drupal.alertTypesPlugins.addPlugin('example', {
 *    id: 'example',
 *    isPermitted: function (alert) {
 *
 *    },
 *    process: function (alert) {
 *
 *    },
 *    onload: function(alert) {
 *
 *    },
 *    bindEvents: function (alert) {
 *
 *    }
 *  });
 **/
(function ($, window, Drupal, drupalSettings) {
  Drupal.alertTypesPlugins.addPlugin('example', {

    id: 'example',

    isPermitted: function (alert) {

    },

    bypassLimit: function (alert) {

    },

    process: function (alert) {

    },

    onLoad: function(alert) {

    },

    bindEvents: function (alert) {

    }

  });

})(jQuery, window, Drupal, drupalSettings);
