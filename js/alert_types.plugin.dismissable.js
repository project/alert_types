/**
 *  Dismissable Alert Plugin.
 **/
(function ($, window, Drupal, drupalSettings) {
  Drupal.alertTypesPlugins.addPlugin('dismissable', {

    // The plugin name.
    id: 'dismissable',

    // Plugin specific settings.
    settings: {
      'speed': 500,
    },

    isPermitted: function (alert) {
      var id = $(alert).attr('id');
      if (this.getStatus(id) === 'dismissed') {
        return false;
      }
      return true;
    },

    bindEvents: function (alert) {
      var _this = this;
      var $closeButton = $(alert).find('.alert--close');
      var id = $(alert).attr('id');

      $closeButton.on('click', function (e) {
        e.preventDefault();
        $(alert).slideUp(_this.settings.speed, function (e) {
          _this.setStatus(id, 'dismissed');
        });
      });
    },
});

})(jQuery, window, Drupal, drupalSettings);
