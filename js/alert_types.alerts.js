/**
 * @file
 **/

(function ($, window, Drupal, drupalSettings, cookies) {

  Drupal.behaviors.AlertsService = {
    attach: function attach(context) {
      const elements = once('alert-types', '[data-alert-types]', context);
      if (elements.length) {
        elements.forEach(function () {
          let AlertTypes = new Drupal.alertTypes(elements, drupalSettings.alerts);

          AlertTypes.initialize()

          let url = AlertTypes.createAjaxUrl();
          AlertTypes.ajaxRequest(url);
        });
      }
    }
  };

  /**
   * This object is for storing and retrieving plugins. Plugins are
   * used to control and customize alert behavior.
   */
  Drupal.alertTypesPlugins = {

    plugins: {},

    // Initialize the alert plugin with some helper methods.
    // This make sit much easier to interact with the status of the
    // alert.
    initializePlugin: function(plugin) {

      plugin.getStatus = function(id) {
        return cookies.get(id);
      }

      plugin.setStatus = function(id, status) {
        cookies.set(id, status)
      }

      return plugin;
    },

    addPlugin: function(name, plugin) {
      if (!this.plugins.hasOwnProperty(name)) {
        this.plugins[name] = {};
      }
      this.plugins[name] = this.initializePlugin(plugin);
    },

    getPlugin: function(name) {
      return this.plugins[name];
    },

    getAllPlugins: function() {
      return this.plugins;
    }
  }

  /**
   * Constructor for alert types.
   *
   * @param context
   *   The current dom context.
   * @param alerts
   *   Array of alert ids.
   */
  Drupal.alertTypes = function (context, alerts) {

    this.plugins = Drupal.alertTypesPlugins.getAllPlugins();

    this.alerts = alerts;

    this.context = context;
  }

  /**
   * Initialize for the alert types.
   */
  Drupal.alertTypes.prototype.initialize = function () {

    // By default this should be hidden and only show if alerts are found.
    $(this.context).hide();

    // If alerts are found then make sure that they show.
    if (this.alerts.length > 0) {
      $(this.context).show();
    }
  }

  /**
   * Execute the Ajax request.
   *
   * @returns {string}
   *   The URL endpoint with data as the query parameters.
   */
  Drupal.alertTypes.prototype.ajaxRequest = function (url) {
    var _this = this;

    $.ajax({
      url: url,
      method: "GET",
      headers: {
        'Content-Type': "application/vnd.api+json",
        'Accept': 'application/vnd.api+json"',
      },

      success: function (response, status, xhr) {
        let data = response.data;
        let renderedAlerts = [];

        // Remove any items that are no longer permitted. For example, they may have been
        // dismissed.
        let filteredData = data.filter(function(item) {
          if (_this.alerts.includes(item.id)) {
            if (item.content) {
              // Turn the string into a dom element.
              let doc = new DOMParser().parseFromString(item.content, "text/html");
              let dom = doc.body.firstChild;

              // Determine whether we should show this alert or not. If it's not
              // permitted it will go to the next alert. If no plugins return false then
              // assume true.
              var show = _this.callPluginMethod('isPermitted', dom);
              if (typeof show == 'undefined') {
                show = true
              }
            }
            return show;
          }
        });

        // Render the permitted alerts.
        filteredData.forEach(function(item) {
          // Turn the string into a dom element.
          let doc = new DOMParser().parseFromString(item.content, "text/html");
          let dom = doc.body.firstChild;

          // Let plugins manipulate the dom of the response.
          // @todo add support for multiple plugins.
          _this.callPluginMethod('process', dom);

          // Allow plugins to bypass the single alert limit.
          var bypassLimit = _this.callPluginMethod('bypassLimit', dom);
          if (typeof bypassLimit == 'undefined') {
            bypassLimit = false
          }

          // If no alerts have been rendered or bypass limit is set then show the alert.
          if (renderedAlerts.length === 0 || bypassLimit) {
            // Convert the dom back to string to be embedded.
            if (dom) {
              item.content = dom.outerHTML;
            }

            $(_this.context).append(item.content);
            renderedAlerts.push(item.id);
          }
        });
      },
      complete: function (xhr, status) {
        $(_this.context).find('.alert').each(function (i, e) {
          _this.callPluginMethod('onLoad', e);
          _this.callPluginMethod('bindEvents', e);
        });
      }
    });
  }

  /**
   * Dynamically invoke all plugins implementing a method. This also supports dynamic arguments.
   *
   * @param method
   *   The method name being invoked.
   */
  Drupal.alertTypes.prototype.callPluginMethod = function (method) {
    let _this = this;
    let result;
    for (var name in _this.plugins) {
      var plugin = _this.plugins[name];
      if (typeof plugin[method] === 'function') {
        // This dynamically passes in the function arguments.
        result = plugin[method].apply(plugin, Array.prototype.slice.call(arguments, 1));
      }
    }
    return result;
  }

  /**
   * Helper function for creating the AJAX url.
   */
  Drupal.alertTypes.prototype.createAjaxUrl = function () {
    // We're hashing the current path for cache busting during requests.
    var path = "/" + drupalSettings.path.currentPath;
    var hash = 0, i, chr;
    for (i = 0; i < path.length; i++) {
      chr = path.charCodeAt(i);
      hash = ((hash << 5) - hash) + chr;
      // Convert to 32bit integer.
      hash |= 0; //
    }
    var params = {
      'v': hash,
    };

    var query = $.param(params, true);
    return window.location.protocol + "//" + window.location.host + '/alerts/json?' + query;
  }

})(jQuery, window, Drupal, drupalSettings, window.Cookies);
