/**
 *  Dismiss Timer Alert Plugin.
 **/
(function ($, window, Drupal, drupalSettings) {
  Drupal.alertTypesPlugins.addPlugin('dismiss_timer', {

    id: 'dismiss_timer',

    settings: {
      'speed': 500,
      'timer': 0,
    },

    onLoad: function (alert) {
      this.settings.timer =  $(alert).data('dismiss-timer') * 1000;
    },

    isPermitted: function (alert) {
      var id = $(alert).attr('id');
      if (this.getStatus(id) === 'dismissed') {
        return false;
      }
      return true;
    },

    bindEvents: function (alert) {
      var _this = this;
      var id = $(alert).attr('id');

      if (this.settings.timer > 0) {
        setTimeout(function () {
          $(alert).slideUp(_this.settings.speed, function (e) {
            _this.setStatus(id, 'dismissed');
          });
        }, (_this.settings.timer))
      }
    },
});

})(jQuery, window, Drupal, drupalSettings);
