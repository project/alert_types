INTRODUCTION
------------

Alert Types gives you the ability display different types of alerts anywhere
on your site.

Some of the major features include:

 * The ability to define different alert types using Drupal’s bundle system.
 * Each alert type is fieldable allowing you to extend it to create your own functionality.
 * Drag and drop functionality allowing you to prioritize each alert.
 * Optional user dismiss or auto dismiss.
 * Visibility rules supporting paths, bundles or roles.
 * Behavior plugins and javascript API to allow you easily and seamlessly implement your own customizations (see below for more details).

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

HOW TO USE
------------
1. Enable this module like normal.
2. Go to the block management section and place the Alerts block into a region.
3. Go to /admin/structure to create a new Alert Type
4. Go to /admin/content/alerts to create a new alert.


TECHNICAL DETAILS
------------
Alerts are rendered via AJAX and accommodate cache contexts and paths. When an alert is dismissed, it gets stored as a cookie the so that it won’t appear again for that user.

Behavior Plugins
------------
Behavior plugins allow you to control the display and behavior of your alerts. For example, both Dismissable and Dismiss Timer are plugins.

### Creating a custom Alert behavior.
To add a custom behavior do the following:
1. Create a new AlertType javascript plugin and library.
2. Create a new AlertTypeBehavior Drupal plugin.
3. Enable the behavior on the Alert Type.

Create a new javascript library and plugin.
```javascript
(function ($, window, Drupal, drupalSettings) {
  Drupal.alertTypesPlugins.addPlugin('play_sound', {

    id: 'play_sound',

    isPermitted: function (alert) {
      // Perform logic to determine whther you want it to show.
    },

    process: function(alert) {
      // Procss the alert dom before rendering it.
    },

    onLoad: function (alert) {
      // Perform any operations on load.
    },

    bindEvents: function (alert) {
      // Bind any custom events to the alert.
    },
});

})(jQuery, window, Drupal, drupalSettings);

```

Add this file to a library.
```
dismissable:
    js:
      js/play_sound.js: { }
    dependencies:
      - alert_types/alerts

```
Next create an AlertTypeBehavior plugin.
```php
/**
 * Add the play sound behavior.
 *
 * @AlertTypeBehavior(
 *  id = "play_sound",
 *  label = @Translation("Play sound"),
 *  description = @Translation("Play a sound when user hovers their cursor."),
 *  library = "module/play_sound",
 * )
 */
class PlaySoundOnHover extends AlertTypeBehaviorBase {

}
```

Finally, edit the Alert Type that you want to support this behavior and click its checkbox to turn it on. This feature will
now be available alert authors.
